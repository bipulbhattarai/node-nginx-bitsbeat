******TO run the app clone the git repo **

- - cd node-nginx-bitsbeat
- sudo docker build -t bitsbeat/node .
- sudo docker run -d -p 4444:4444 --name nodeapp bitsbeat/node
- sudo docker build -t bitsbeat/nginx .
- sudo docker run -d -p 80:80 --link nodeapp --name nginx-bitsbeat bitsbeat/nginx
- type localhost in your browser to see app running.
